$(function() {
    $("form[name='inputForm']").validate({
        rules: {
            name: "required",
            username: "required",
            email: {
                required: true,
                email: true
            },
            street: "required",
            suite: "required",
            city: "required",
            zipcode: {
                required: true,
                number: true
            },
            geoLat: {
                required: true,
                number: true
            },
            geoLng: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 4
            },
            website: {
                required: true,
                url: true
            },
            companyName: "required",
            companyPhrase: "required",
            companyBs: "required"
        },

        messages: {
            name: {
                required: "&nbsp; Please enter your name"
            },
            username: "&nbsp; Please enter your username",
            email: {
                required: "&nbsp; Please enter your e-mail",
                email: "&nbsp; Please enter a valid email address"
            },
            street: "&nbsp; Please enter your street",
            suite: "&nbsp; Please enter your suite",
            city: "&nbsp; Please enter your city",
            zipcode: {
                required: "&nbsp; Please enter your zipcode"
            },
            geoLat: {
                required: "&nbsp; Please enter your latitude"
            },
            geoLng: {
                required: "&nbsp; Please enter your longtitude"
            },
            phone: {
                required: "&nbsp; Please enter your phone number",
                minlength: "&nbsp; Phone numbers consist of at least 4 numbers"
            },
            website: {
                required: "&nbsp; Please enter your website",
                url: "&nbsp; Please enter a valid URL"
            },
            companyName: {
                required: "&nbsp; Please enter your company name"
            },
            companyPhrase: {
                required: "&nbsp; Please enter your company phrase"
            },
            companyBs: {
                required: "&nbsp; Please enter your company Bs"
            }
        },

        submitHandler: function() {
            var url = "http://jsonplaceholder.typicode.com/users";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#userForm").serialize(),
                success: function(data) {
                    new PNotify({
                        title: 'Success!',
                        text: 'The user has beed successfully added.',
                        type: 'success'
                    });
                    $('#closeBtn').trigger('click');
                },
                error: function(data) {
                    new PNotify({
                        title: 'Uhhhh',
                        text: 'Something went wrong',
                        type: 'error'
                    });
                }
            });
        }
        
    });
});